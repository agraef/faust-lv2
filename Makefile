# This is a GNU Makefile.

# Package name and version:
dist = faust-lv2-$(version)
version = 1.4

# Installation prefix and default installation dirs. NOTE: lv2libdir is used
# to install the plugins, bindir and faustlibdir for the Faust-related tools
# and architectures. You can also set these individually if needed.
prefix = /usr/local
bindir = $(prefix)/bin
libdir = $(prefix)/lib
datadir = $(prefix)/share
lv2libdir = $(libdir)/lv2
faustlibdir = $(datadir)/faust

# Try to guess the Faust installation prefix.
faustprefix = $(patsubst %/bin/faust,%,$(shell which faust 2>/dev/null))
ifeq ($(strip $(faustprefix)),)
# Fall back to /usr/local.
faustprefix = /usr/local
endif
incdir = $(faustprefix)/include
faustincdir = $(incdir)/faust

# qmake setup (for GUI compilation). You may have to set this explicitly if
# the qmake executable isn't on PATH, and also if you need to choose a
# specific Qt version to make your favorite DAW happy. (At the time of this
# writing, Ardour still uses Qt4, while the latest Qtractor versions require
# Qt5. Carla will work with either.)
qmake=$(shell which qmake-qt5 || which /opt/local/libexec/qt5/bin/qmake || which qmake-qt4 || which /opt/local/libexec/qt4/bin/qmake || echo qmake)

# Determine the Qt version so that we can edit the manifests accordingly.
qtversion = $(shell $(qmake) -v 2>/dev/null | tail -1 | sed 's/.*Qt version \([0-9]\).*/\1/')

# The URI prefix and bundle dir of the generated plugins. Adjust as needed.
uriprefix = https://faustlv2.bitbucket.io
bundledir = faust.lv2

# Set this variable to build the plugin GUIs. This option requires Qt4 or Qt5
# and an LV2 host which supports the LV2 UI extension.
gui = 0

# Set this variable to create plugins with dynamic manifests instead of static
# manifest files (the latter is the default). This requires an LV2 host with
# support for dynamic manifests.
dyn-manifests = 0

# Pick the proper manifest template according to the settings above.
ifeq ($(dyn-manifests),0)
staticman = -static
endif
ifneq ($(gui),0)
uiman = ui
endif
manifest = lv2$(uiman)-manifest$(staticman)-template.ttl

# Make sure that this gets included in the compilation command if you change
# the URI prefix from the default.
DEFINES = -DURI_PREFIX='"$(uriprefix)"' -DDLLEXT='"$(DLL)"'

# Here are a few conditional compilation directives which you can set.
# Disable Faust metadata.
#DEFINES += -DFAUST_META=0
# Disable MIDI controller processing.
#DEFINES += -DFAUST_MIDICC=0
# Disable the tuning control (synth only).
#DEFINES += -DFAUST_MTS=0
# Disable polyphony/tuning controls on GUI.
#DEFINES += -DVOICE_CTRLS=0
# Number of voices (synth: polyphony).
#DEFINES += -DNVOICES=16
# Debug recognized MIDI controller metadata.
#DEFINES += -DDEBUG_META=1
# Debug incoming MIDI messages.
#DEFINES += -DDEBUG_MIDI=1
# Debug MIDI note messages (synth).
#DEFINES += -DDEBUG_NOTES=1
# Debug MIDI controller messages.
#DEFINES += -DDEBUG_MIDICC=1
# Debug RPN messages (synth: pitch bend range, master tuning).
#DEFINES += -DDEBUG_RPN=1
# Debug MTS messages (synth: octave/scale tuning).
#DEFINES += -DDEBUG_MTS=1

# This is set automatically according to the gui option.
ifneq ($(gui),0)
DEFINES += -DFAUST_UI=1
endif

# GUI configuration. Adjust as needed/wanted.
# Uncomment this to get OSC and/or HTTP support in the plugin GUIs.
#UI_DEFINES += -DOSCCTRL -DHTTPCTRL
# Uncomment this to also get the QR code popup for HTTP.
#UI_DEFINES += -DQRCODECTRL
# Uncomment this to set a special style sheet. This must be the basename of
# one of the style sheets available in $(faustincdir)/gui/Styles.
#STYLE = Grey

# Add the libraries needed for the UI options above.
ifneq "$(findstring -DOSCCTRL,$(UI_DEFINES))" ""
UI_LIBS += -lOSCFaust
endif
ifneq "$(findstring -DHTTPCTRL,$(UI_DEFINES))" ""
UI_LIBS += -lHTTPDFaust -lmicrohttpd
endif
ifneq "$(findstring -DQRCODECTRL,$(UI_DEFINES))" ""
UI_LIBS += -lqrencode
endif

ifneq "$(STYLE)" ""
UI_DEFINES += -DSTYLE=$(STYLE)
RESOURCES = $(faustincdir)/gui/Styles/$(STYLE).qrc
endif

# Uncomment this to keep the Qt GUI projects after compilation.
#KEEP = true

# Default compilation flags.
CFLAGS = -O3
# Use this for debugging output instead.
#CFLAGS = -g -O2

# Shared library suffix and compiler option to create a shared library.
DLL = .so
shared = -shared

# Try to guess the host system type and figure out platform specifics.
host = $(shell ./config.guess)
ifneq "$(findstring -mingw,$(host))" ""
# Windows (untested)
EXE = .exe
DLL = .dll
endif
ifneq "$(findstring -darwin,$(host))" ""
# OSX
DLL = .dylib
shared = -dynamiclib
# MacPorts compatibility
EXTRA_CFLAGS += -I/opt/local/include
endif
ifneq "$(findstring x86_64-,$(host))" ""
# 64 bit, needs -fPIC flag
EXTRA_CFLAGS += -fPIC
endif
ifneq "$(findstring x86,$(host))" ""
# architecture-specific options for x86 and x86_64
EXTRA_CFLAGS += -msse -ffast-math
endif

# DSP sources and derived files.
dspsrc    = $(sort $(wildcard */*.dsp))
cppsrc	  = $(dspsrc:.dsp=.cpp)
plugins	  = $(dspsrc:.dsp=$(DLL))
ifneq ($(gui),0)
uicppsrc  = $(dspsrc:.dsp=ui.cpp)
uiplugins = $(dspsrc:.dsp=ui$(DLL))
endif
manifests = $(dspsrc:.dsp=-manifest.ttl)
ifeq ($(dyn-manifests),0)
ttl	  = $(dspsrc:.dsp=.ttl)
progs	  = $(dspsrc:.dsp=$(EXE))
endif

# Bundle names for split bundles.
bundles   = $(notdir $(dspsrc:.dsp=))

all: bundle

info:
	@echo "FAUST-LV2 BUILD CONFIGURATION:"
	@echo
ifneq ($(dyn-manifests),0)
	@echo "dynamic manifests:    ON"
else
	@echo "dynamic manifests:    OFF"
endif
ifneq ($(gui),0)
	@echo "plugin GUIs:          ON"
else
	@echo "plugin GUIs:          OFF"
endif
ifneq ($(strip $(qtversion)),)
	@echo "Qt version:           $(qtversion)"
else
	@echo "Qt version:           NOT DETECTED"
endif
	@echo "Manifest template:    $(manifest)"
	@echo "Installation prefix:  $(prefix)"
	@echo "Faust prefix:         $(faustprefix)"
	@echo
	@echo "CFLAGS  = $(CFLAGS) $(EXTRA_CFLAGS)"
	@echo "DEFINES = $(DEFINES) $(UI_DEFINES)"
ifneq ($(strip $(UI_LIBS)),)
	@echo "UI_LIBS = $(UI_LIBS)"
endif
ifneq ($(strip $(DESTDIR)),)
	@echo "DESTDIR = $(DESTDIR)"
endif
	@echo
	@echo "Run 'make' to build, 'make install' to install under $(prefix)."
	@echo "Split plugin bundles: Run 'make split-bundle' and 'make install-split'."
	@echo "Other useful targets: 'clean', 'uninstall', 'dist'."
	@echo "Maintainers: Use 'prefix' for install prefix, 'DESTDIR' for staging."

# This creates a single bundle from all the plugins.
bundle: $(plugins) $(uiplugins) $(progs) $(ttl) $(manifests)
	rm -Rf lv2
	mkdir -p lv2/$(bundledir)
	cat $(manifests) > lv2/$(bundledir)/manifest.ttl
	cp $(plugins) $(uiplugins) $(ttl) lv2/$(bundledir)

# This creates split bundles (one LV2 bundle per plugin).
split-bundle: $(plugins) $(uiplugins) $(progs) $(ttl) $(manifests)
	rm -Rf lv2
ifeq ($(dyn-manifests),0)
	for x in $(bundles); do	mkdir -p lv2/$$x.lv2 && cp */$$x*$(DLL) */$$x.ttl lv2/$$x.lv2 && cp */$$x-manifest.ttl lv2/$$x.lv2/manifest.ttl; done
else
	for x in $(bundles); do	mkdir -p lv2/$$x.lv2 && cp */$$x*$(DLL) lv2/$$x.lv2 && cp */$$x-manifest.ttl lv2/$$x.lv2/manifest.ttl; done
endif

# Use this to create just the C++ source, plugins and/or manifests.
cppsrc: $(cppsrc) $(uicppsrc)
plugins: $(plugins) $(uiplugins)
manifests: $(ttl) $(manifests)

# Generic build rules.

# GUI part
%ui.cpp: %.dsp lv2ui.cpp
	faust -a lv2ui.cpp -cn $(notdir $(@:%ui.cpp=%)) -I examples $< -o $@

ifndef KEEP
KEEP = false
endif

# need to invoke qmake here -- you must have Qt installed to make this work
%ui$(DLL): %ui.cpp
	+(tmpdir=$(dir $@)$(notdir $(<:%.cpp=%.src)); rm -rf $$tmpdir; mkdir -p $$tmpdir; cp $< $$tmpdir; cd $$tmpdir; $(qmake) -project -t lib -o "$(notdir $(<:%.cpp=%.pro))" "CONFIG += gui plugin no_plugin_name_prefix warn_off" "QT += widgets printsupport network" "INCLUDEPATH+=$(CURDIR)" "INCLUDEPATH+=.." "INCLUDEPATH+=$(faustincdir)" "QMAKE_CXXFLAGS=$(EXTRA_CFLAGS) $(CFLAGS) $(UI_DEFINES) $(shell echo $(DEFINES)|sed -e 's/"/\\\\\\\\\\\\\\"/g')" "LIBS+=$(UI_LIBS)" "HEADERS+=$(CURDIR)/lv2qtgui.h" "HEADERS+=$(faustincdir)/gui/faustqt.h" "RESOURCES+=$(RESOURCES)"; $(qmake) *.pro && make && cp $(notdir $@) .. && cd $(CURDIR) && ($(KEEP) || rm -rf $$tmpdir))

# Plugin part. Make sure that this comes after the GUI part. (Apparently older
# GNU make versions require that the special case rules come first.)
%.cpp: %.dsp lv2.cpp
	faust -a lv2.cpp -cn $(notdir $(@:%.cpp=%)) -I examples $< -o $@

%$(DLL): %.cpp
	$(CXX) $(shared) $(EXTRA_CFLAGS) $(CFLAGS) $(DEFINES) -Iexamples $< -o $@

# Executable to generate the dynamic part of the manifest.
%: %.cpp
	$(CXX) $(EXTRA_CFLAGS) $(CFLAGS) $(DEFINES) -Iexamples $< -o $@

# Static manifest part.
%-manifest.ttl: $(manifest)
	sed -e 's?@uri@?$(uriprefix)/$(notdir $(@:%-manifest.ttl=%))?g' -e 's?@name@?$(notdir $(@:%-manifest.ttl=%))?g' -e 's?@dllext@?$(DLL)?g' -e 's?ui:Qt5UI?ui:Qt$(qtversion)UI?g' < $< > $@

# Dynamic manifest part.
%.ttl: %$(EXE)
	$< > $@

# Clean.

clean:
	rm -Rf lv2 $(dspsrc:.dsp=ui.src) $(cppsrc) $(dspsrc:.dsp=ui.cpp) $(plugins) $(dspsrc:.dsp=ui$(DLL)) $(progs) $(ttl) $(manifests)

# Install.

install:
	test -d $(DESTDIR)$(lv2libdir) || mkdir -p $(DESTDIR)$(lv2libdir)
	cp -R lv2/$(bundledir) $(DESTDIR)$(lv2libdir)

uninstall:
	rm -Rf $(DESTDIR)$(lv2libdir)/$(bundledir)

# Use this if you want to install the split-bundle version instead.

install-split:
	test -d $(DESTDIR)$(lv2libdir) || mkdir -p $(DESTDIR)$(lv2libdir)
	cp -R $(addprefix lv2/, $(addsuffix .lv2, $(bundles))) $(DESTDIR)$(lv2libdir)

uninstall-split:
	rm -Rf $(addprefix $(DESTDIR)$(lv2libdir)/, $(addsuffix .lv2, $(bundles)))

# Use this to install the Faust architectures and scripts included in this
# package over an existing Faust installation.

install-faust:
	test -d $(DESTDIR)$(bindir) || mkdir -p $(DESTDIR)$(bindir)
	cp faust2lv2 $(DESTDIR)$(bindir)
	test -d $(DESTDIR)$(faustlibdir) || mkdir -p $(DESTDIR)$(faustlibdir)
	cp lv2.cpp lv2ui.cpp lv2qtgui.h $(DESTDIR)$(faustlibdir)

uninstall-faust:
	rm -f $(addprefix $(DESTDIR)$(bindir)/, faust2lv2)
	rm -f $(addprefix $(DESTDIR)$(faustlibdir)/, lv2.cpp lv2ui.cpp lv2qtgui.h)

# Roll a distribution tarball.

DISTFILES = COPYING COPYING.LESSER Makefile README.md config.guess faust2lv2 *.cpp *.h *.ttl examples/*.dsp examples/*.lib examples/*.h

dist:
	rm -rf $(dist)
	for x in $(dist) $(addprefix $(dist)/, examples); do mkdir $$x; done
	for x in $(DISTFILES); do ln -sf "$$PWD"/$$x $(dist)/$$x; done
	rm -f $(dist).tar.bz2
	tar cfjh $(dist).tar.bz2 $(dist)
	rm -rf $(dist)

distcheck: dist
	tar xfj $(dist).tar.bz2
	cd $(dist) && make && make install DESTDIR=./BUILD
	rm -rf $(dist)
